from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import CreateForm, ItemForm

# from django.contrib.auth.decorators import login_required


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": detail,
    }
    return render(request, "todos/detail.html", context)


# @login_required
def todo_list_create(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id=todos.id)
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = CreateForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
        return redirect("todo_list_detail", id=update.id)
    else:
        form = CreateForm(instance=update)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/itemscreate.html", context)


def todo_item_update(request, id):
    update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=update)
        if form.is_valid():
            update = form.save()
        return redirect("todo_list_detail", id=update.list.id)
    else:
        form = ItemForm(instance=update)

    context = {"form": form}

    return render(request, "todos/itemedit.html", context)
