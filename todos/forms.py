from django.forms import ModelForm
from .models import TodoList, TodoItem


class CreateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class ItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
